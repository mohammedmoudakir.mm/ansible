Role Name
=========

	Ce rôle permet d'installer le PLI postgresql (par defaut 9.6.8) sur une version linux

Requirements
------------

	Avoir déposé le binaire du PLI dans le repertoire files au niveau du playbook

Role Variables
--------------

	# vars file for orange.cb.pgsql-install 
	ocbpgsqlVersion: "9.6.8"                                            ==> Version de postgresql
	ocbpgsqlVersionGoroco: "CE968-RDG01R00C01"
	ocbpgsqlSignature: "PA-PGQ-{{ ocbpgsqlVersionGoroco }}.SIG"         ==> Fichier signature du pli
	ocbpgsqlPackage: "PA-PGQ-{{ ocbpgsqlVersionGoroco }}.tar"           ==> Fichier tar du PLI a deployer
	ocbpgsqlPathInstallation: "/images/pgsql/{{ ocbpgsqlVersion }}"     ==> Repertoire ou sera extrait le PLI
	ocbpgsqlBinaryShell: "pgsql-install.ksh"                            ==> binaire d'installation du PLI

Example Playbook
----------------


    - hosts: servers
      roles:
          - orange.cb.pgsql-install

    - hosts: servers
      roles:
          # En surchargeant
          - orange.cb.pgsql-install
            ocbpgsqlVersion: "X.Y.Z"
            ocbpgsqlVersionGoroco: "CEXYZ-RDG99R99C99"


Author Information
------------------

	Roles initialises par le CCA : Orange/OF/DTSI/DESI/DIXSI/AIF/CCA
	Sharepoint: http://shp.itn.ftgroup/sites/CCA/default.aspx

